#include <memory>

#include "process_information.h"

#include <unistd.h>

#include <chrono>
#include <iostream>
#include <thread>

int main( int /*argc*/, char** /*argv*/ )
{
	using namespace std::chrono_literals;
	std::thread t( [] {
		ProcessInformation p( getpid() );
		while ( true ) {
			std::this_thread::sleep_for( 1000ms / 25 );
			std::cout << '\r' << p.getCpuUsage().second << ' ' << memory_cast< megabytes >( p.getRamUsage().second ).count() << std::flush;
		}
	} );
	std::thread t1( [] {
		while ( true ) {
		}
	} );
	while ( true ) {
	}
	return 0;
}
