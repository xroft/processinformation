#include "process_information.h"

#include <cstring>
#include <iterator>
#include <mutex>
#include <string>
#include <vector>

#include <unistd.h>

int ProcessInformation::_processors_number = 1;

ProcessInformation::ProcessInformation( int pid ) : _pid( pid ), _process_stat_file( processDirectory() + "/stat" )
{
	auto timeSample = getTimeSample();
	_last_cpu = timeSample.cpu;
	_last_sys_cpu = timeSample.sys_cpu;
	_last_user_cpu = timeSample.user_cpu;

	static std::once_flag get_processors_number_flag;
	std::call_once( get_processors_number_flag, []() {
		std::ifstream cpuinfo_file( _cpuinfo_filename );
		for ( std::string line; !cpuinfo_file.eof(); std::getline( cpuinfo_file, line ) ) {
			if ( line.find( "processor" ) != std::string::npos ) {
				_processors_number++;
			}
		}
	} );
}

std::pair< std::chrono::steady_clock::time_point, double > ProcessInformation::getCpuUsage() const
{
	double result = -1.0;
	auto timeSample = getTimeSample();
	if ( !( timeSample.cpu <= _last_cpu || timeSample.sys_cpu < _last_sys_cpu || timeSample.user_cpu < _last_user_cpu ) ) {
		result = ( timeSample.sys_cpu - _last_sys_cpu ) + ( timeSample.user_cpu - _last_user_cpu );
		result /= ( timeSample.cpu - _last_cpu ).count();
		result /= _processors_number;
		result *= decltype( _last_cpu )::duration::period::ratio::den;
	}
	_last_cpu = timeSample.cpu;
	_last_sys_cpu = timeSample.sys_cpu;
	_last_user_cpu = timeSample.user_cpu;

	return {std::chrono::steady_clock::now(), result};
}

std::pair< std::chrono::steady_clock::time_point, bytes > ProcessInformation::getRamUsage() const
{
	if ( _process_stat_file.is_open() ) {
		_process_stat_file.clear();
		_process_stat_file.seekg( 0 );
		std::vector< std::string > words{std::istream_iterator< std::string >{_process_stat_file}, std::istream_iterator< std::string >{}};
		if ( words.size() > 14 ) {
			return {std::chrono::steady_clock::now(), bytes( std::stoull( words[ 23 ] ) * static_cast< uint64_t >( getpagesize() ) )};
		}
	}
	return {std::chrono::steady_clock::now(), bytes( 0 )};
}

std::string ProcessInformation::processDirectory() const
{
	return "/proc/" + std::to_string( _pid );
}

ProcessInformation::cpu_times ProcessInformation::getTimeSample() const
{
	if ( _process_stat_file.is_open() ) {
		_process_stat_file.clear();
		_process_stat_file.seekg( 0 );
		std::vector< std::string > words{std::istream_iterator< std::string >{_process_stat_file}, std::istream_iterator< std::string >{}};
		if ( words.size() > 14 ) {
			return {std::chrono::steady_clock::now(), std::stoll( words[ 13 ] ), std::stoll( words[ 14 ] )};
		}
	}
	return {std::chrono::steady_clock::now(), 0, 0};
}
