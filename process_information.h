#ifndef PROCESS_INFORMATION_H
#define PROCESS_INFORMATION_H

#include <chrono>
#include <cstdint>
#include <ctime>
#include <fstream>

using bits = std::chrono::duration< uint64_t, std::ratio< 1, 8 > >;
using bytes = std::chrono::duration< uint64_t, std::ratio< 1, 1 > >;
using kilobytes = std::chrono::duration< double, std::ratio< 1024, 1 > >;
using megabytes = std::chrono::duration< double, std::ratio< 1024 * 1024, 1 > >;
using gigabytes = std::chrono::duration< double, std::ratio< 1024 * 1024 * 1024, 1 > >;
template < typename _ToDur, typename _Rep, typename _Period >
constexpr _ToDur memory_cast( const std::chrono::duration< _Rep, _Period >& __d )
{
	return std::chrono::duration_cast< _ToDur >( __d );
}

class ProcessInformation
{
public:
	explicit ProcessInformation( int pid );

	std::pair< std::chrono::steady_clock::time_point, double > getCpuUsage() const;
	std::pair< std::chrono::steady_clock::time_point, bytes > getRamUsage() const;

private:
	static inline constexpr const char* _cpuinfo_filename = "/proc/cpuinfo";
	std::string processDirectory() const;

	struct cpu_times
	{
		std::chrono::steady_clock::time_point cpu;
		clock_t sys_cpu;
		clock_t user_cpu;
	};

	cpu_times getTimeSample() const;

	const int _pid = 0;
	mutable std::ifstream _process_stat_file;

	mutable std::chrono::steady_clock::time_point _last_cpu;
	mutable clock_t _last_sys_cpu;
	mutable clock_t _last_user_cpu;

	static int _processors_number;
};

#endif	// PROCESS_INFORMATION_H
